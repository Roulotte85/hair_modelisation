clear all ;
euler

l=1:5:700;
n_image=length(l);
fig=figure;
for idx=1:n_image
    pcolor((C(:,:,l(idx))./mask.*mask)');colorbar;caxis([0 1]);
    hold on
    quiver(x(5:5:Nx),y(5:5:Ny),u_graph',v_graph','w')
    hold off
    drawnow
    frame = getframe(fig);
    im{idx} = frame2im(frame);
end
close;
figure;
