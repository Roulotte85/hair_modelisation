close all
image=imread ('pixhyeres1.png'); 
long_min=6.140754;
long_max= 6.560000;
lat_min=42.96000;
lat_max=43.158476;
deltlong=long_max-long_min;
deltlat=lat_max-lat_min;

paslong=deltlong/200;
paslat=deltlat/136;

Long=[43.0963889,43.08138896,43.08,43.078333,43.0769444,43.07669444,43.0775,43.0758333,43.081667];
Lat=[5.947778,6.0111,5.9986111,5.9836111,5.9698333,6.007778,6.00083333,6.0016666,6.015];

Long2=[];
Lat2=[];
for i=1:length(Long)
    Long2(i)=long_max-(Long(i)-long_min);
    Lat2(i)=lat_max-(Lat(i)-lat_min);
end

  

DistLong=[];
DistLat=[];

for i=1:200
    DistLong(i)=((i*paslong)+long_min);
end


for i=1:136
    DistLat(i)=-((i*paslat)-lat_max);
    
end

imagesc(DistLong,DistLat,image);
grid on
xlabel('Longitude','Fontsize',14)
ylabel('Latitude','Fontsize',14)
set(gca,'YDir','normal','Fontsize',14)
set(gca,'Xdir','normal')



% hg 43.158476, 6.140754
% 
% hd 43.158476, 6.560000
% 
% bg 42.960000, 6.140754
% 
% bd 42.960000, 6.560000
mask=zeros(136,200);

for j=1:200
    for i=1:136
        a(i,j)=I(i,j,1);
        b(i,j)=I(i,j,2);
        c(i,j)=I(i,j,3);
        if a(i,j)==170 & b(i,j)==218 & c(i,j)==255;
            %if a(i,j)==165 & b(i,j)==212 & c(i,j)==248; 
            mask(i,j)=1;
        end
    end
end
mask=imrotate(mask,-90);
save test.mat

    