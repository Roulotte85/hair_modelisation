%Lagrange
clear xL yL uL vL

Nt=150;
dt=150000;
Lat2=roundn(500*Lat-3075,0);
Lon2=roundn(715.79*Lon-30750,0);
Lon2(find(Lon2==0))=1;
Lat2(find(Lat2==0))=1;
l=length(Lat);
for ip=1:l
    xL(1,ip)=(Lat2(ip))*dx;%En m
    yL(1,ip)=(Lon2(ip))*dy;%en m
end

% uL(1)=u(xL(1)/dx,yL(1)/dy);
% vL(1)=v(xL(1)/dx,yL(1)/dy);

   for ip=1:l
    uL(1,ip)=u(Lat2(ip),Lon2(ip));
    vL(1,ip)=v(Lat2(ip),Lon2(ip));
end

for ip=1:l
    for l=2:Nt-1
    xL(l,ip)=xL(l-1,ip)+dt*uL(l-1,ip);
    yL(l,ip)=yL(l-1,ip)+dt*vL(l-1,ip);
    uL(l,ip)=interp2(u',xL(l,ip)/dx,yL(l,ip)/dy);%Interpolation entre deux points de la grille
    vL(l,ip)=interp2(v',xL(l,ip)/dx,yL(l,ip)/dy);
    end
end

figure(1)
colormap(hsv)
pcolor((Phi./mask.*mask)')
hold on
plot(xL/dx,yL/dy,uL,vL,'color','k','LineWidth',2)
hold on
quiver(x(5:5:Nx),y(5:5:Ny),u_graph',v_graph','w','linewidth',2)
hold on
plot(Lat2,Lon2,'*  ','color','r','MarkerSize',8)
xlabel('Latitude')
ylabel('Longitude')
title('Trac� calcul� et r�el du d�placement d une nappe de p�trole au large de Toulon')
hold off

p=findobj('type','line');
p=p(end:-1:1);
 
str={'Trac�s calcul�s' 'Lignes de courant en surface' 'Trac� r�el'};
 
idx=[1];
 
legend(p(idx),str(idx));





